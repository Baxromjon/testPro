import {BASEURL, TOKEN} from "./constant";
import axios from "axios"

export default (params) => {
    let url = BASEURL + params.url;
    let method = params.method;
    let data = params.data;
    // let headers = params.headers;

    return axios({
        url,
        method,
        data,
        // headers: {
        //     "Authorization": "Bearer " + TOKEN
        //
        // }
        headers: {
            "Authorization": "Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIxIiwiaWF0IjoxNjMwMjk2MTk1LCJleHAiOjE2MzA5MDA5OTV9.HYF5UQCbFoDMhqAoZdlX_UhZa5UvLUSEmR87mggPSknsbakiVToa1EGD7f4zh8NhFK0mAZiNPnYYHtsvXGjhXg"

        }
    })
}